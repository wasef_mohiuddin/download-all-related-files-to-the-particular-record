//Created by: Wasef Mohiuddin
//Purpose: This method is to enable downloading the documents attached to the current record.
 
@AuraEnabled
public static String downloadFiles(Id recordId){
 List<Id> setOfFilesId=new List<Id>();
 List<ContentDocumentLink> listOfFilesId=[SELECT ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN (SELECT Id from Object__c where Id =: recordId)];
        for(ContentDocumentLink contentDocumentVariable:listOfFilesId){
            setOfFilesId.add(contentDocumentVariable.ContentDocumentId);
        }
        for(Id id:setOfFilesId)
        {
        }
        if(!setOfFilesId.isEmpty()) {
            String downLoadUrl = String.format(String.isBlank(URL.getSalesforceBaseUrl().toExternalForm()) ? '/{0}{1}?' : URL.getSalesforceBaseUrl().toExternalForm() + '/{0}{1}?', 
                                               new String[]{'sfc/servlet.shepherd/version/download/', String.join(setOfFilesId, '/')
                                                   }
                                              );
            return downLoadUrl;
        }
        return null;
    }

